/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.example.samplestickerapp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.example.samplestickerapp.util.ClickEvent;
import com.example.samplestickerapp.util.LoadMoreStickers;
import com.example.samplestickerapp.util.MyEventBus;
import com.example.samplestickerapp.util.StickersLoaded;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;


import com.example.samplestickerapp.util.MyLog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import com.google.android.gms.ads.AdRequest;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;


public class StickerPackListActivity extends AddStickerPackActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String EXTRA_STICKER_PACK_LIST_DATA = "sticker_pack_list";
    private static final int STICKER_PREVIEW_DISPLAY_LIMIT = 5;
    private final StickerPackListAdapter.OnAddButtonClickedListener onAddButtonClickedListener = pack -> {
        addStickerPackToWhatsApp(pack.identifier, pack.name);
    };
    private LinearLayoutManager packLayoutManager;
    private RecyclerView packRecyclerView;
    private StickerPackListAdapter allStickerPacksListAdapter;
    private WhiteListCheckAsyncTask whiteListCheckAsyncTask;
    private ArrayList<StickerPack> stickerPackList;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private int counter;
    private SearchView mSearchView;
    private MenuItem mSearch;
    private View layout_suggests;
    private View layout_about;
    private Toolbar toolbar;

    private int counterClicks = 0;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        MyLog.i("####### click #########");

        counterClicks++;
        if (counterClicks > 10) {
            // Toast.makeText(this, "intertitial 10 clicks", Toast.LENGTH_SHORT).show();
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            counterClicks = 0;
        }

    }

    @Subscribe
    public void clickEvent(ClickEvent event) {
        MyLog.i("####### click event #########");
        // Toast.makeText(this, "intertitial pack aberto", Toast.LENGTH_SHORT).show();
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewGroup v = findViewById(R.id.main_layout);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyLog.i("click");
            }
        });

        packRecyclerView = findViewById(R.id.sticker_pack_list);
        layout_suggests = findViewById(R.id.layout_suggests);
        layout_about = findViewById(R.id.layout_about);
        stickerPackList = getIntent().getParcelableArrayListExtra(EXTRA_STICKER_PACK_LIST_DATA);

        View layout = findViewById(R.id.layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;

                if (counter > 5) {
                    mInterstitialAd.show();
                    counter = 0;
                }
            }
        });


        showStickerPackList(stickerPackList);

        MobileAds.initialize(this, "ca-app-pub-5876317032514043~8759909819");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-5876317032514043/4629093114");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        /*mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);*/

        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle("Início");
        setSupportActionBar(this.toolbar);

        this.toolbar.setBackgroundColor(getResources().getColor(R.color.infoPageBackgroundColor));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);


        this.toolbar.setTitle("Início");

        MyEventBus.getInstance().register(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        whiteListCheckAsyncTask = new WhiteListCheckAsyncTask(this);
        whiteListCheckAsyncTask.execute(stickerPackList.toArray(new StickerPack[stickerPackList.size()]));

        this.clearSearchView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.toolbar.setTitle("Início");
        if (whiteListCheckAsyncTask != null && !whiteListCheckAsyncTask.isCancelled()) {
            whiteListCheckAsyncTask.cancel(true);
        }
    }

    private void showStickerPackList(List<StickerPack> stickerPackList) {
        allStickerPacksListAdapter = new StickerPackListAdapter(stickerPackList, onAddButtonClickedListener);

        packRecyclerView.setAdapter(allStickerPacksListAdapter);
        packLayoutManager = new LinearLayoutManager(this);
        packLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                packRecyclerView.getContext(),
                packLayoutManager.getOrientation()
        );
        packRecyclerView.addItemDecoration(dividerItemDecoration);
        packRecyclerView.setLayoutManager(packLayoutManager);
        packRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this::recalculateColumnCount);

        packRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                LinearLayoutManager layoutManager = ((LinearLayoutManager) packRecyclerView.getLayoutManager());
                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int lastCompleteVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();

                LoadMoreStickers event = new LoadMoreStickers();
                event.first = firstVisiblePosition;
                event.current = lastCompleteVisiblePosition;
                event.size = stickerPackList.size();

                MyEventBus.getInstance().post(event);

                counter++;


                if (allStickerPacksListAdapter.getClicks() > 5) {
                    mInterstitialAd.show();
                    allStickerPacksListAdapter.setClicks(0);
                }

            }
        });
    }

    private void recalculateColumnCount() {
        final int previewSize = getResources().getDimensionPixelSize(R.dimen.sticker_pack_list_item_preview_image_size);
        int firstVisibleItemPosition = packLayoutManager.findFirstVisibleItemPosition();
        StickerPackListItemViewHolder viewHolder = (StickerPackListItemViewHolder) packRecyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
        if (viewHolder != null) {
            final int max = Math.max(viewHolder.imageRowView.getMeasuredWidth() / previewSize, 1);
            int numColumns = Math.min(STICKER_PREVIEW_DISPLAY_LIMIT, max);
            allStickerPacksListAdapter.setMaxNumberOfStickersInARow(numColumns);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /////////////////////////////////////////////////////////////////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        this.mSearch = menu.findItem(R.id.action_search);

        this.mSearchView = (SearchView) mSearch.getActionView();
        this.mSearchView.setQueryHint("Busca de Adesivos");
        this.mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                ArrayList<StickerPack> filter = new ArrayList<>();
                for (StickerPack sticker : stickerPackList) {
                    if (sticker.name.toLowerCase().contains(s.toLowerCase()) ||
                            sticker.publisher.contains(s.toLowerCase())) {
                        filter.add(sticker);
                    }
                }

                showStickerPackList(filter);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {


                return false;
            }
        });

        ImageView closeButton = (ImageView) this.mSearchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showStickerPackList(stickerPackList);


                EditText et = (EditText) findViewById(R.id.search_src_text);

                clearSearchView();
            }
        });


        return true;
    }

    private void clearSearchView() {

        if (this.mSearchView != null) {
            EditText et = (EditText) this.mSearchView.findViewById(R.id.search_src_text);

            //Clear the text from EditText view
            et.setText("");

            //Clear query
            this.mSearchView.setQuery("", false);
            //Collapse the action view
            this.mSearchView.onActionViewCollapsed();
        }

        if (mSearch != null) {
            //Collapse the search widget
            mSearch.collapseActionView();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    private void enableSearchView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                enableSearchView(child, enabled);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            packRecyclerView.setVisibility(View.VISIBLE);
            layout_suggests.setVisibility(View.GONE);
            layout_about.setVisibility(View.GONE);

            mSearch.setVisible(true);

            this.toolbar.setTitle("Inicio");
            showStickerPackList(stickerPackList);
        } else if (id == R.id.my_stickers) {
            packRecyclerView.setVisibility(View.VISIBLE);
            layout_suggests.setVisibility(View.GONE);
            layout_about.setVisibility(View.GONE);

            mSearch.setVisible(false);

            ArrayList<StickerPack> filter = new ArrayList<>();
            for (StickerPack sticker : stickerPackList) {
                if (sticker.getIsWhitelisted()) {
                    filter.add(sticker);
                }
            }

            this.toolbar.setTitle("Meus Stickers");
            showStickerPackList(filter);
        } else if (id == R.id.suggest) {
            layout_suggests.setVisibility(View.VISIBLE);
            layout_about.setVisibility(View.GONE);
            packRecyclerView.setVisibility(View.GONE);

            this.toolbar.setTitle("Sugestões");
            mSearch.setVisible(false);

        } else if (id == R.id.about) {
            layout_suggests.setVisibility(View.GONE);
            layout_about.setVisibility(View.VISIBLE);
            packRecyclerView.setVisibility(View.GONE);

            this.toolbar.setTitle("Sobre o App");
            mSearch.setVisible(false);

        } else if (id == R.id.rate) {
            final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                    .threshold(3)
                    .title("Gostou dos nossos adesivos?. Avalie nosso aplicativo")
                    .positiveButtonText("Depois")
                    .formCancelText("Cancelar")
                    .formSubmitText("Enviar")
                    .formHint("Diga-nos o que precisamos melhorar.")
                    .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                        @Override
                        public void onFormSubmitted(String feedback) {
                            // Toast.makeText(StickerPackListActivity.this, feedback, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .build();

            ratingDialog.show();

        } else {
            packRecyclerView.setVisibility(View.GONE);
            // enableSearchView(mSearchView, false);
            mSearch.setVisible(false);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Subscribe
    public void stickersLoaded(StickersLoaded event) {
        MyLog.i("############# stickers loaded: " + event + " ################");

        ArrayList<StickerPack> stickers = StickerPackLoader.fetchStickerPacks(this);

        if (stickers.size() != stickerPackList.size()) {
            showStickerPackList(stickers);
            packRecyclerView.getLayoutManager().scrollToPosition(stickers.size() - 1);
        }


    }

    static class WhiteListCheckAsyncTask extends AsyncTask<StickerPack, Void, List<StickerPack>> {
        private final WeakReference<StickerPackListActivity> stickerPackListActivityWeakReference;

        WhiteListCheckAsyncTask(StickerPackListActivity stickerPackListActivity) {
            this.stickerPackListActivityWeakReference = new WeakReference<>(stickerPackListActivity);
        }

        @Override
        protected final List<StickerPack> doInBackground(StickerPack... stickerPackArray) {
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity == null) {
                return Arrays.asList(stickerPackArray);
            }
            for (StickerPack stickerPack : stickerPackArray) {
                stickerPack.setIsWhitelisted(WhitelistCheck.isWhitelisted(stickerPackListActivity, stickerPack.identifier));
            }
            return Arrays.asList(stickerPackArray);
        }

        @Override
        protected void onPostExecute(List<StickerPack> stickerPackList) {
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity != null) {
                stickerPackListActivity.allStickerPacksListAdapter.setStickerPackList(stickerPackList);
                stickerPackListActivity.allStickerPacksListAdapter.notifyDataSetChanged();
            }
        }


    }
}
