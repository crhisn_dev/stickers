/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.example.samplestickerapp;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.example.samplestickerapp.util.LoadMoreStickers;
import com.example.samplestickerapp.util.MyEventBus;
import com.example.samplestickerapp.util.MyLog;
import com.example.samplestickerapp.util.StickersLoaded;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class StickerContentProvider extends ContentProvider {

    /**
     * Do not change the strings listed below, as these are used by WhatsApp. And changing these will break the interface between sticker app and WhatsApp.
     */
    public static final String STICKER_PACK_IDENTIFIER_IN_QUERY = "sticker_pack_identifier";
    public static final String STICKER_PACK_NAME_IN_QUERY = "sticker_pack_name";
    public static final String STICKER_PACK_PUBLISHER_IN_QUERY = "sticker_pack_publisher";
    public static final String STICKER_PACK_ICON_IN_QUERY = "sticker_pack_icon";
    public static final String ANDROID_APP_DOWNLOAD_LINK_IN_QUERY = "android_play_store_link";
    public static final String IOS_APP_DOWNLOAD_LINK_IN_QUERY = "ios_app_download_link";
    public static final String PUBLISHER_EMAIL = "sticker_pack_publisher_email";
    public static final String PUBLISHER_WEBSITE = "sticker_pack_publisher_website";
    public static final String PRIVACY_POLICY_WEBSITE = "sticker_pack_privacy_policy_website";
    public static final String LICENSE_AGREENMENT_WEBSITE = "sticker_pack_license_agreement_website";

    public static final String STICKER_FILE_NAME_IN_QUERY = "sticker_file_name";
    public static final String STICKER_FILE_EMOJI_IN_QUERY = "sticker_emoji";
    // public static final String CONTENT_FILE_NAME = "com/example/samplestickerapp/util/contents.json";

    public static Uri AUTHORITY_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(BuildConfig.CONTENT_PROVIDER_AUTHORITY).appendPath(StickerContentProvider.METADATA).build();

    /**
     * Do not change the values in the UriMatcher because otherwise, WhatsApp will not be able to fetch the stickers from the ContentProvider.
     */
    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    static final String METADATA = "metadata";
    private static final int METADATA_CODE = 1;

    private static final int METADATA_CODE_FOR_SINGLE_PACK = 2;

    static final String STICKERS = "stickers";
    private static final int STICKERS_CODE = 3;

    static final String STICKERS_ASSET = "stickers_asset";
    private static final int STICKERS_ASSET_CODE = 4;

    private static final int STICKER_PACK_TRAY_ICON_CODE = 5;

    private List<StickerPack> stickerPackList;

    private String authority = null;

    @Override
    public boolean onCreate() {
        this.authority = BuildConfig.CONTENT_PROVIDER_AUTHORITY;
        if (!authority.startsWith(Objects.requireNonNull(getContext()).getPackageName())) {
            throw new IllegalStateException("your authority (" + authority + ") for the content provider should start with your package name: " + getContext().getPackageName());
        }

        this.loadURIs();

        MyEventBus.getInstance().register(this);
        MyLog.i("%%%%%%%% create %%%%%%%%%%%");

        return true;
    }

    private void loadURIs() {
        //the call to get the metadata for the sticker packs.
        MATCHER.addURI(authority, METADATA, METADATA_CODE);

        //the call to get the metadata for single sticker pack. * represent the identifier
        MATCHER.addURI(authority, METADATA + "/*", METADATA_CODE_FOR_SINGLE_PACK);

        //gets the list of stickers for a sticker pack, * respresent the identifier.
        MATCHER.addURI(authority, STICKERS + "/*", STICKERS_CODE);

        for (StickerPack stickerPack : getStickerPackList()) {
            MATCHER.addURI(authority, STICKERS_ASSET + "/" + stickerPack.identifier + "/" + stickerPack.trayImageFile, STICKER_PACK_TRAY_ICON_CODE);
            for (Sticker sticker : stickerPack.getStickers()) {
                MATCHER.addURI(authority, STICKERS_ASSET + "/" + stickerPack.identifier + "/" + sticker.imageFileName, STICKERS_ASSET_CODE);
            }
        }

        MyLog.i("%%%%%%%% load URIs %%%%%%%%%%%");
    }

    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        MyLog.i("calling query: " + uri);

        this.loadURIs();

        final int code = MATCHER.match(uri);

        if (code == METADATA_CODE) {
            return getPackForAllStickerPacks(uri);
        } else if (code == METADATA_CODE_FOR_SINGLE_PACK) {
            return getCursorForSingleStickerPack(uri);
        } else if (code == STICKERS_CODE) {
            return getStickersForAStickerPack(uri);
        } else {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public AssetFileDescriptor openAssetFile(@NonNull Uri uri, @NonNull String mode) {
        final int matchCode = MATCHER.match(uri);
        if (matchCode == STICKERS_ASSET_CODE || matchCode == STICKER_PACK_TRAY_ICON_CODE) {
            return getImageAsset(uri);
        }
        return null;
    }


    @Override
    public String getType(@NonNull Uri uri) {
        final int matchCode = MATCHER.match(uri);
        switch (matchCode) {
            case METADATA_CODE:
                return "vnd.android.cursor.dir/vnd." + BuildConfig.CONTENT_PROVIDER_AUTHORITY + "." + METADATA;
            case METADATA_CODE_FOR_SINGLE_PACK:
                return "vnd.android.cursor.item/vnd." + BuildConfig.CONTENT_PROVIDER_AUTHORITY + "." + METADATA;
            case STICKERS_CODE:
                return "vnd.android.cursor.dir/vnd." + BuildConfig.CONTENT_PROVIDER_AUTHORITY + "." + STICKERS;
            case STICKERS_ASSET_CODE:
                return "image/webp";
            case STICKER_PACK_TRAY_ICON_CODE:
                return "image/png";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }


    private synchronized void readContentFile(@NonNull Context context) {
        final File cacheFile = getContext().getExternalCacheDir();

        try {
            File contentjson = new File(cacheFile, "mycontents.json");
            MyLog.i("loading: " + contentjson.getAbsolutePath() + " exists: " + contentjson.exists());

            if (contentjson.exists()) {
                FileInputStream fis = new FileInputStream(contentjson);
                stickerPackList = ContentFileParser.parseStickerPacks(fis);
            } else {
                stickerPackList = new ArrayList<>();
            }
        } catch (Exception e) {
            MyLog.e(e);
        }


        /*try (InputStream contentsInputStream = context.getAssets().open(CONTENT_FILE_NAME)) {
            stickerPackList = ContentFileParser.parseStickerPacks(contentsInputStream);
        } catch (IOException | IllegalStateException e) {
            throw new RuntimeException(CONTENT_FILE_NAME + " file has some issues: " + e.getMessage(), e);
        }*/
    }

    public List<StickerPack> getStickerPackList() {
        readContentFile(Objects.requireNonNull(getContext()));
        return stickerPackList;
    }

    private Cursor getPackForAllStickerPacks(@NonNull Uri uri) {
        return getStickerPackInfo(uri, getStickerPackList());
    }

    private Cursor getCursorForSingleStickerPack(@NonNull Uri uri) {
        final String identifier = uri.getLastPathSegment();
        for (StickerPack stickerPack : getStickerPackList()) {
            if (identifier.equals(stickerPack.identifier)) {
                return getStickerPackInfo(uri, Collections.singletonList(stickerPack));
            }
        }

        return getStickerPackInfo(uri, new ArrayList<>());
    }

    @NonNull
    private Cursor getStickerPackInfo(@NonNull Uri uri, @NonNull List<StickerPack> stickerPackList) {
        MatrixCursor cursor = new MatrixCursor(
                new String[]{
                        STICKER_PACK_IDENTIFIER_IN_QUERY,
                        STICKER_PACK_NAME_IN_QUERY,
                        STICKER_PACK_PUBLISHER_IN_QUERY,
                        STICKER_PACK_ICON_IN_QUERY,
                        ANDROID_APP_DOWNLOAD_LINK_IN_QUERY,
                        IOS_APP_DOWNLOAD_LINK_IN_QUERY,
                        PUBLISHER_EMAIL,
                        PUBLISHER_WEBSITE,
                        PRIVACY_POLICY_WEBSITE,
                        LICENSE_AGREENMENT_WEBSITE
                });

        for (StickerPack stickerPack : stickerPackList) {
            MatrixCursor.RowBuilder builder = cursor.newRow();
            builder.add(stickerPack.identifier);
            builder.add(stickerPack.name);
            builder.add(stickerPack.publisher);
            builder.add(stickerPack.trayImageFile);
            builder.add(stickerPack.androidPlayStoreLink);
            builder.add(stickerPack.iosAppStoreLink);
            builder.add(stickerPack.publisherEmail);
            builder.add(stickerPack.publisherWebsite);
            builder.add(stickerPack.privacyPolicyWebsite);
            builder.add(stickerPack.licenseAgreementWebsite);
        }

        cursor.setNotificationUri(Objects.requireNonNull(getContext()).getContentResolver(), uri);
        return cursor;
    }

    @NonNull
    private Cursor getStickersForAStickerPack(@NonNull Uri uri) {
        final String identifier = uri.getLastPathSegment();
        MatrixCursor cursor = new MatrixCursor(new String[]{STICKER_FILE_NAME_IN_QUERY, STICKER_FILE_EMOJI_IN_QUERY});
        for (StickerPack stickerPack : getStickerPackList()) {
            if (identifier.equals(stickerPack.identifier)) {
                for (Sticker sticker : stickerPack.getStickers()) {
                    cursor.addRow(new Object[]{sticker.imageFileName, TextUtils.join(",", sticker.emojis)});
                }
            }
        }
        cursor.setNotificationUri(Objects.requireNonNull(getContext()).getContentResolver(), uri);
        return cursor;
    }

    private AssetFileDescriptor getImageAsset(Uri uri) throws IllegalArgumentException {
        AssetManager am = Objects.requireNonNull(getContext()).getAssets();
        final List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() != 3) {
            throw new IllegalArgumentException("path segments should be 3, uri is: " + uri);
        }
        String fileName = pathSegments.get(pathSegments.size() - 1);
        final String identifier = pathSegments.get(pathSegments.size() - 2);
        if (TextUtils.isEmpty(identifier)) {
            throw new IllegalArgumentException("identifier is empty, uri: " + uri);
        }
        if (TextUtils.isEmpty(fileName)) {
            throw new IllegalArgumentException("file name is empty, uri: " + uri);
        }
        //making sure the file that is trying to be fetched is in the list of stickers.
        for (StickerPack stickerPack : getStickerPackList()) {
            if (identifier.equals(stickerPack.identifier)) {
                if (fileName.equals(stickerPack.trayImageFile)) {
                    return fetchFile(uri, am, fileName, identifier);
                } else {
                    for (Sticker sticker : stickerPack.getStickers()) {
                        if (fileName.equals(sticker.imageFileName)) {
                            return fetchFile(uri, am, fileName, identifier);
                        }
                    }
                }
            }
        }
        return null;
    }


    private AssetFileDescriptor fetchFile(@NonNull Uri uri, @NonNull AssetManager am, @NonNull String fileName, @NonNull String identifier) {
        MyLog.i("identifier: " + identifier);
        try {
            final File cacheFile = getContext().getExternalCacheDir();
            final File dirIdentifier = new File(cacheFile, identifier);
            final File file = new File(dirIdentifier, fileName);

            MyLog.i("file: " + file.getAbsolutePath() + " exists: " + file.exists());

            if (file.exists()) {
                return new AssetFileDescriptor(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY), 0, AssetFileDescriptor.UNKNOWN_LENGTH);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private AssetFileDescriptor fetchFile_Orig(@NonNull Uri uri, @NonNull AssetManager am, @NonNull String fileName, @NonNull String identifier) {
        try {
            return am.openFd(identifier + "/" + fileName);
        } catch (IOException e) {
            Log.e(Objects.requireNonNull(getContext()).getPackageName(), "IOException when getting asset file, uri:" + uri, e);
            return null;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }


    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    final long ONE_MEGABYTE = 1024 * 1024;


    private int lastCompleteVisiblePosition;
    private int morePages = 10;
    private int qtde;

    private boolean called = false;

    @Subscribe
    public void loadMore(LoadMoreStickers event) {
        if (!called) {
            MyLog.i("############# load more event: " + event.current + " ################");

            this.lastCompleteVisiblePosition = event.current;

            morePages += morePages;

            called = true;
            this.loadFromFirebase(event);
        }

    }


    private void loadFromFirebase(LoadMoreStickers event) {
        final File cacheFile = getContext().getExternalCacheDir();

        MyLog.i(cacheFile.getAbsolutePath());

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://centralstrickers.appspot.com");

        StorageReference ref = storage.getReference().child("contents.json");
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                try {
                    MyLog.i("downloading: " + bytes.length);
                    File contentjson = new File(cacheFile, "mycontents.json");
                    FileOutputStream fos = new FileOutputStream(contentjson);
                    fos.write(bytes);
                    fos.close();

                    FileInputStream fis = new FileInputStream(contentjson);
                    final List<StickerPack> packs = ContentFileParser.parseStickerPacks(fis);

                    if (packs.size() <= event.size) {
                        called = false;
                        MyLog.i("###### skiping loading #####");
                        return;
                    }


                    for (int i = 0; i < Math.min(packs.size(), qtde); i++) {

                        StickerPack pack = packs.get(i);
                        downloadFile(cacheFile, pack.identifier, pack.trayImageFile);

                        List<Sticker> stickers = pack.getStickers();
                        MyLog.i("identifier: " + pack.identifier);
                        for (Sticker sticker : stickers) {
                            MyLog.i("sticker: " + sticker.imageFileName);
                            downloadFile(cacheFile, pack.identifier, sticker.imageFileName);
                        }
                    }

                    AsyncTask<Void, Void, Void> check = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {


                            while (true) {

                                boolean all_downloaded = true;

                                for (StickerPack pack : packs) {
                                    List<Sticker> stickers = pack.getStickers();
                                    for (Sticker sticker : stickers) {

                                        File dirIdentifier = new File(cacheFile, pack.identifier);
                                        File fileContent = new File(dirIdentifier, sticker.imageFileName);

                                        MyLog.i("file to down: " + fileContent.getAbsolutePath() + " exists: " + fileContent.exists());
                                        all_downloaded = all_downloaded && fileContent.exists();


                                    }


                                    File dirIdentifier = new File(cacheFile, pack.identifier);
                                    File fileContent = new File(dirIdentifier, pack.trayImageFile);

                                    MyLog.i("file to down: " + fileContent.getAbsolutePath() + " exists: " + fileContent.exists());
                                    all_downloaded = all_downloaded && fileContent.exists();
                                }

                                if (!all_downloaded) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (Exception e) {

                                    }
                                } else {
                                    break;
                                }

                            }

                            MyLog.i("saiu");
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {

                            StickersLoaded ll = new StickersLoaded();
                            MyEventBus.getInstance().post(ll);
                            called = false;
                        }
                    };
                    check.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                } catch (Exception e) {
                    MyLog.e(e);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                MyLog.e(exception);
            }
        });
    }


    private void downloadFile(File cacheFile, String identifier, String fileName) {
        File dirIdentifier = new File(cacheFile, identifier);
        if (!dirIdentifier.exists()) {
            dirIdentifier.mkdir();
        }

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://centralstrickers.appspot.com");

        StorageReference ref = storage.getReference().child(identifier + "/" + fileName);
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                try {
                    File fileContent = new File(dirIdentifier, fileName);
                    MyLog.i("downloading: " + fileContent.getAbsolutePath());

                    FileOutputStream fos = new FileOutputStream(fileContent);
                    fos.write(bytes);
                    fos.close();
                } catch (Exception e) {
                    MyLog.e(e);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                MyLog.e(exception);
            }
        });
    }


}
