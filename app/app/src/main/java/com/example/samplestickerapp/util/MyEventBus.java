package com.example.samplestickerapp.util;

import com.google.common.eventbus.EventBus;

public class MyEventBus {


    private static EventBus instance;

    public static EventBus getInstance(){
        if(instance == null){
            instance = new EventBus();
        }
        return instance;
    }



}
