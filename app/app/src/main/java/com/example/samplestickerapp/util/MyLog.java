package com.example.samplestickerapp.util;

import android.util.Log;

public class MyLog {

    public static void i(String str){
        Log.i("ca_app", str);
    }

    public static void e(String str, Exception e){
        Log.e("ca_app", str, e);
    }

    public static void e(Exception e){
        Log.e("ca_app", e.getLocalizedMessage(), e);
    }
}
