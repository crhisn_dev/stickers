/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.example.samplestickerapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samplestickerapp.util.MyLog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.ads.MobileAds;



public class EntryActivity extends BaseActivity {
    final long ONE_MEGABYTE = 1024 * 1024;
    private View progressBar;
    private LoadListAsyncTask loadListAsyncTask;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        overridePendingTransition(0, 0);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        progressBar = findViewById(R.id.entry_activity_progress);


        MobileAds.initialize(this, "ca-app-pub-6326266416509376~6693027983");
        MobileAds.initialize(this, "ca-app-pub-6326266416509376/9823368605");


        this.loadFromFirebase();

        // loadListAsyncTask = new LoadListAsyncTask(EntryActivity.this);
        //loadListAsyncTask.execute();
    }

    private void loadFromFirebase() {
        final File cacheFile = this.getExternalCacheDir();

        MyLog.i(cacheFile.getAbsolutePath());

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://centralstrickers.appspot.com");

        StorageReference ref = storage.getReference().child("contents.json");
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                try {
                    MyLog.i("downloading: " + bytes.length);
                    File contentjson = new File(cacheFile, "mycontents.json");
                    FileOutputStream fos = new FileOutputStream(contentjson);
                    fos.write(bytes);
                    fos.close();



                    FileInputStream fis = new FileInputStream(contentjson);

                    final List<StickerPack> packs = ContentFileParser.parseStickerPacks(fis);

                    // final List<StickerPack> tmp = ContentFileParser.parseStickerPacks(fis);
                    //  final List<StickerPack> packs = tmp.subList(0, Math.min(3, tmp.size()));
                    for (StickerPack pack : packs) {
                        downloadFile(cacheFile, pack.identifier, pack.trayImageFile);

                        List<Sticker> stickers = pack.getStickers();
                        MyLog.i("identifier: " + pack.identifier);
                        for (Sticker sticker : stickers) {
                            MyLog.i("sticker: " + sticker.imageFileName);
                            downloadFile(cacheFile, pack.identifier, sticker.imageFileName);
                        }
                    }

                    AsyncTask<Void, Void, Void> check = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {


                            while (true) {

                                boolean all_downloaded = true;

                                for (StickerPack pack : packs) {
                                    List<Sticker> stickers = pack.getStickers();
                                    for (Sticker sticker : stickers) {

                                        File dirIdentifier = new File(cacheFile, pack.identifier);
                                        File fileContent = new File(dirIdentifier, sticker.imageFileName);

                                        MyLog.i("file to down: " + fileContent.getAbsolutePath() + " exists: " + fileContent.exists());
                                        all_downloaded = all_downloaded && fileContent.exists();


                                    }


                                    File dirIdentifier = new File(cacheFile, pack.identifier);
                                    File fileContent = new File(dirIdentifier, pack.trayImageFile);

                                    MyLog.i("file to down: " + fileContent.getAbsolutePath() + " exists: " + fileContent.exists());
                                    all_downloaded = all_downloaded && fileContent.exists();
                                }

                                if (!all_downloaded) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (Exception e) {

                                    }
                                } else {
                                    break;
                                }

                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            loadListAsyncTask = new LoadListAsyncTask(EntryActivity.this);
                            loadListAsyncTask.execute();
                        }
                    };
                    check.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } catch (Exception e) {
                    MyLog.e(e);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                MyLog.e(exception);
            }
        });
    }

    private void downloadFile(File cacheFile, String identifier, String fileName) {
        File dirIdentifier = new File(cacheFile, identifier);
        if (!dirIdentifier.exists()) {
            dirIdentifier.mkdir();
        }

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://centralstrickers.appspot.com");

        StorageReference ref = storage.getReference().child(identifier + "/" + fileName);
        ref.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                try {
                    File fileContent = new File(dirIdentifier, fileName);
                    MyLog.i("downloading: " + fileContent.getAbsolutePath());

                    FileOutputStream fos = new FileOutputStream(fileContent);
                    fos.write(bytes);
                    fos.close();
                } catch (Exception e) {
                    MyLog.e(e);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                MyLog.e(exception);
            }
        });
    }

    public void downloadFiles(File cacheFile) {


    }

    private void showStickerPack(ArrayList<StickerPack> stickerPackList) {
        progressBar.setVisibility(View.GONE);
       /* if (stickerPackList.size() > 1) {
            final Intent intent = new Intent(this, StickerPackListActivity.class);
            intent.putParcelableArrayListExtra(StickerPackListActivity.EXTRA_STICKER_PACK_LIST_DATA, stickerPackList);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        } else {
            final Intent intent = new Intent(this, StickerPackDetailsActivity.class);
            intent.putExtra(StickerPackDetailsActivity.EXTRA_SHOW_UP_BUTTON, false);
            intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_DATA, stickerPackList.get(0));
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }*/
        final Intent intent = new Intent(this, StickerPackListActivity.class);
        intent.putParcelableArrayListExtra(StickerPackListActivity.EXTRA_STICKER_PACK_LIST_DATA, stickerPackList);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    private void showErrorMessage(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        Log.e("EntryActivity", "error fetching sticker packs, " + errorMessage);
        final TextView errorMessageTV = findViewById(R.id.error_message);
        errorMessageTV.setText(getString(R.string.error_message, errorMessage));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadListAsyncTask != null && !loadListAsyncTask.isCancelled()) {
            loadListAsyncTask.cancel(true);
        }
    }

    static class LoadListAsyncTask extends AsyncTask<Void, Void, Pair<String, ArrayList<StickerPack>>> {
        private final WeakReference<EntryActivity> contextWeakReference;

        LoadListAsyncTask(EntryActivity activity) {
            this.contextWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected Pair<String, ArrayList<StickerPack>> doInBackground(Void... voids) {
            ArrayList<StickerPack> stickerPackList;

            try {
                final Context context = contextWeakReference.get();
                if (context != null) {
                    stickerPackList = StickerPackLoader.fetchStickerPacks(context);
                    if (stickerPackList.size() == 0) {
                        return new Pair<>("could not find any packs", null);
                    }
                    for (StickerPack stickerPack : stickerPackList) {
                        StickerPackValidator.verifyStickerPackValidity(context, stickerPack);
                    }
                    return new Pair<>(null, stickerPackList);
                } else {
                    return new Pair<>("could not fetch sticker packs", null);
                }
            } catch (Exception e) {
                Log.e("EntryActivity", "error fetching sticker packs", e);
            }

            return new Pair<>("error after try again", null);
        }

        @Override
        protected void onPostExecute(Pair<String, ArrayList<StickerPack>> stringListPair) {

            final EntryActivity entryActivity = contextWeakReference.get();
            if (entryActivity != null) {
                if (stringListPair.first != null) {
                    entryActivity.showErrorMessage(stringListPair.first);
                } else {
                    entryActivity.showStickerPack(stringListPair.second);
                }
            }

        }
    }
}
